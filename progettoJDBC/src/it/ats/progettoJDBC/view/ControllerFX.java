package it.ats.progettoJDBC.view;


import java.util.List;

import it.ats.progettoJDBC.model.Corsista;
import it.ats.progettoJDBC.persistence.DaoCorsista;
import it.ats.progettoJDBC.persistence.DaoException;
import it.ats.progettoJDBC.persistence.impl.DaoCorsistaImpl;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class ControllerFX {
	// CREATE
	@FXML
	private TextField nome;
	@FXML
	private TextField cognome;

	// REMOVE
	@FXML
	private TextField idRemove;
	// OUTPUT
	@FXML
	private Label outputText;

	// UPDATE
	@FXML
	private TextField nomeUp;
	@FXML
	private TextField cognomeUp;
	@FXML
	private TextField idFind;

	private DaoCorsista daoCorsista;

	@FXML
	private AnchorPane anchor;
	@FXML
	private ScrollPane dynamicScroller;
	
	/**
	 * The constructor. The constructor is called before the initialize() method.
	 */
	public ControllerFX() {
	}

	/**
	 * Initializes the controller class. This method is automatically called after
	 * the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		daoCorsista = new DaoCorsistaImpl();
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */
	public void setMainApp(CorsistiFX mainApp) {
	}

	public void aggiungiCorsista() {
		try {
			Corsista corsista = new Corsista(nome.getText(), cognome.getText());
			System.out.println(corsista);
			daoCorsista.insert(corsista);
			outputText.setText("Corsista " + nome.getText() + " " + cognome.getText() + " inserito con successo");

		} catch (DaoException e) {
			System.out.println("daoexception");
			outputText.setText("erroreDB: " + e.getMessage());
			e.printStackTrace();
		} finally {

			nome.setText(null);
			cognome.setText(null);

		}

	}

	public void vediCorsisti() {

		List<Corsista> corsisti = null;

		try {
			corsisti = daoCorsista.findAll();
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String result = "";
		result += "corsisti: \n";
		int cont=0;
		for (Corsista corsista2 : corsisti) {
			result += corsista2 + "\n";
			cont++;
		}
		
		this.outputText.setMaxHeight((17*cont));
		this.outputText.setPrefHeight(outputText.getMaxHeight());
		this.anchor.setMaxHeight(outputText.getMaxHeight());
		this.anchor.setPrefHeight(outputText.getMaxHeight());
		this.dynamicScroller.setMaxHeight(outputText.getMaxHeight());
//		this.dynamicScroller.setPrefHeight(outputText.getMaxHeight());
		outputText.setText(result);

	}

	public void rimuoviCorsista() {

		try {
			Corsista corsista = new Corsista(Integer.parseInt(this.idRemove.getText()), nome.getText(),
					cognome.getText());
			daoCorsista.delete(corsista);
		} catch (DaoException e) {
			outputText.setText("errore DB");
			e.printStackTrace();
		}
		outputText.setText("corsista rimosso");

	}

	public void modificaCorsista() {
		try {
			daoCorsista
					.update(new Corsista(Integer.parseInt(idRemove.getText()), nomeUp.getText(), cognomeUp.getText()));
		} catch (NumberFormatException | DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void vediCorsista() {

		Corsista corsista = null;

		try {
			corsista = daoCorsista.findByIdPreparedStatement(Integer.parseInt(idFind.getText()));
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String result = "";
		result += corsista + "\n";

		if(corsista==null) {
			result = "nessun corsista trovato ID non valido";
		}
		outputText.setText(result);

	}

}
