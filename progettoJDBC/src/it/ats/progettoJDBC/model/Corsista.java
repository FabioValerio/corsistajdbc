package it.ats.progettoJDBC.model;

public class Corsista implements Comparable<Corsista> {

	private int id;
	private String nome;
	private String cognome;

	public Corsista(int id, String nome, String cognome) {
		super();
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
	}

	public Corsista(String nome, String cognome) {
		this.nome = nome;
		this.cognome = cognome;
	}

	public Corsista() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	
	public void setId(int id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	@Override
	public String toString() {
		return "Corsista [id=" + id + ", nome=" + nome + ", cognome=" + cognome + "]";
	}

	
	@Override
	public int compareTo(Corsista o) {
		
		return this.id-o.getId();
	}
	
	

}
