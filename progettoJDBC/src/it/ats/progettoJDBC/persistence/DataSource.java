package it.ats.progettoJDBC.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataSource {
	private static DataSource instance = null;

	private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";

	private static final String USER = "scott";

	private static final String PASSWORD = "tiger";

	private static final String URL = "jdbc:oracle:thin:@localhost:1521:corso";

	

	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	private DataSource() {

	}

	public static DataSource getInstance() {
		if (instance == null) {
			instance = new DataSource();
			return instance;
		}
		return instance;
	}

	public Connection getConnection() throws DaoException {
		try {
			Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
			return connection;
		} catch (SQLException e) {

			e.printStackTrace();

			throw new DaoException(e.getMessage());

		}
	}

	public void close(Connection connection) throws DaoException {

		try {
			
			connection.close();
		} catch (SQLException e) {
			
			e.printStackTrace();

			throw new DaoException(e.getMessage(), e);

		}
	}

	public void closeResultSet(ResultSet resultSet) throws DaoException {

		try {
			
			resultSet.close();
		} catch (SQLException e) {
			
			e.printStackTrace();

			throw new DaoException(e.getMessage(), e);

		}
	}
	
	public void closeStatement(Statement statement) throws DaoException {

		try {
			
			statement.close();
		} catch (SQLException e) {
			
			e.printStackTrace();

			throw new DaoException(e.getMessage(), e);

		}
	}
}
