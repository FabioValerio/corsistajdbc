package it.ats.progettoJDBC.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.ats.progettoJDBC.model.Corsista;
import it.ats.progettoJDBC.persistence.DaoCorsista;
import it.ats.progettoJDBC.persistence.DaoException;
import it.ats.progettoJDBC.persistence.DataSource;

public class DaoCorsistaImpl implements DaoCorsista {

	@Override
	public Corsista FindById(long id) throws DaoException {
		DataSource ds = DataSource.getInstance();
		String query = "SELECT * FROM CORSISTA WHERE ID=" + (int) id;
		Connection connection = ds.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		Corsista corsista = null;
		try {
			statement = connection.createStatement();

			resultSet = statement.executeQuery(query);

			if (resultSet != null && resultSet.next()) {
				System.out.println(resultSet.getObject(1));
				System.out.println(resultSet.getString(2));
				System.out.println(resultSet.getString(3));

				corsista = new Corsista(resultSet.getInt(3), resultSet.getString(1), resultSet.getString(2));

			} else {
				System.out.println("resultSet nullo");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ds.closeResultSet(resultSet);
			ds.closeStatement(statement);
			ds.close(connection);

		}

		return corsista;

	}

	@Override
	public List<Corsista> findAll() throws DaoException {
		DataSource ds = DataSource.getInstance();
		String query = "SELECT * FROM CORSISTA";
		Connection connection = ds.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		Corsista corsista = null;
		List<Corsista> corsisti = new ArrayList<>();
		try {
			statement = connection.createStatement();

			resultSet = statement.executeQuery(query);

			while (resultSet != null && resultSet.next()) {
				System.out.println(resultSet.getObject(1));
				System.out.println(resultSet.getString(2));
				System.out.println(resultSet.getString(3));

				corsista = new Corsista(resultSet.getInt(3), resultSet.getString(1), resultSet.getString(2));
				corsisti.add(corsista);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ds.closeResultSet(resultSet);
			ds.closeStatement(statement);
			ds.close(connection);

		}

		Collections.sort(corsisti);
		return corsisti;
	}

	@Override
	public void insert(Corsista corsista) throws DaoException {
		DataSource ds = DataSource.getInstance();
		String query = "INSERT INTO CORSISTA(NOME, COGNOME, ID) VALUES (?,?,UPDATE_SEQUENCE.NEXTVAL)";
				
		System.out.println(query);
		Connection connection = ds.getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(query);
			statement.setString(1, corsista.getNome());
			statement.setString(2, corsista.getCognome());
			
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ds.closeStatement(statement);
			ds.close(connection);
		}


	}

	@Override
	public void delete(Corsista corsista) throws DaoException {
		DataSource ds = DataSource.getInstance();
		String query = "DELETE FROM CORSISTA WHERE ID =" + corsista.getId();
		Connection connection = ds.getConnection();
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeQuery(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ds.closeStatement(statement);
			ds.close(connection);
		}

	}

	@Override
	public void deleteAll() throws DaoException {
		
		DataSource ds = DataSource.getInstance();
		String query = "DELETE FROM CORSISTA";
		Connection connection = ds.getConnection();
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeQuery(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ds.closeStatement(statement);
			ds.close(connection);
		}

	}

	@Override
	public void update(Corsista corsista) throws DaoException {
	
		
		DataSource ds = DataSource.getInstance();
		String query = "UPDATE CORSISTA SET "
				+ "NOME = '"+corsista.getNome()+
				"', COGNOME = '"+corsista.getCognome()+"', "
				+ "WHERE ID ="+corsista.getId()+")";
				
		System.out.println(query);
		Connection connection = ds.getConnection();
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ds.closeStatement(statement);
			ds.close(connection);
		}

		
	}

	@Override
	public Corsista findByIdPreparedStatement(int id) throws DaoException {
		DataSource instance = DataSource.getInstance();
		Connection connection = instance.getConnection();
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		Corsista corsista = null;
		String query = "SELECT *FROM CORSISTA WHERE ID =?";
		
		try {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				corsista = new Corsista();
				
				corsista.setCognome(resultSet.getString("cognome"));
				corsista.setNome(resultSet.getString("nome"));
				corsista.setId(resultSet.getInt("ID"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			instance.closeResultSet(resultSet);
			instance.closeStatement(preparedStatement);
			instance.close(connection);
		}

		return corsista;
	}

}
