package it.ats.progettoJDBC.persistence;

import java.util.List;

import it.ats.progettoJDBC.model.Corsista;

public interface DaoCorsista {

	Corsista FindById(long id) throws DaoException;
	
	List<Corsista> findAll() throws DaoException;
	
	void insert(Corsista corsista)throws DaoException;
	
	void delete(Corsista corsista)throws DaoException;
	
	void deleteAll()throws DaoException;
	
	void update (Corsista corsista)throws DaoException;

	Corsista findByIdPreparedStatement(int id) throws DaoException;
}
