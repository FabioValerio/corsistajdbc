package it.ats.progettoJDBC.persistence;

public class DaoException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4224052713701407870L;

	public DaoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DaoException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public DaoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DaoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
